import React from "react";
import { Link } from "react-router-dom";

function Header() {
  return (
    <header>
      <div className="hero-header">
        <nav style={{ display: "flex", flexDirection: "column" }}>
          <Link to="library">Bibliotek</Link>
          <Link to="mainview">Mina Böcker</Link>
          <Link to="player">Spelare</Link>
        </nav>
      </div>
    </header>
  );
}

export default Header;
