interface IBook {
  id: string;
  title: string;
  author: string;
  liked: boolean;
  // makeLike: (id: string) => void;
}

interface IAudio {
  id: string;
  title: string;
  author: string;
  audioUrl: string;

  // handleClickOpen: () => void;
  // handleClose: () => void;
  // open: boolean;
}

interface ILibraryBook {
  id: string;
  title: string;
  author: string;
  liked: boolean;
  // makeLike: (id: string) => void;
}

export type { IBook, IAudio, ILibraryBook  };
