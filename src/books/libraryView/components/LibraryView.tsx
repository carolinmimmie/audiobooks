import React, { createContext, Dispatch, useEffect, useState } from "react";
import { ILibraryBook } from "../../interfaces";
import Library from "../Library";
import { getBooksStatusFalse } from "./LibraryViewApi";
//Interface för mitt context
interface ILibraryViewContext {
  books: ILibraryBook[];
  setBooks: Dispatch<React.SetStateAction<ILibraryBook[]>>;
}

//Skapa contect
export const LibraryViewContext = createContext<ILibraryViewContext>({
  books: [],
  setBooks: () => {},
});
const LibraryView = () => {
  const [books, setBooks] = useState<ILibraryBook[]>([]);

  const fetchBooks = async () => {
    const unlikedBooks = await getBooksStatusFalse();
    setBooks(unlikedBooks);
  };
  useEffect(() => {
    fetchBooks();
    //and an array of dependencies that make the effect fire.
  }, []);
  // skapar en komponent vars avsikt enbart är att visa upp parents och wrappa den i ett context
  return (
    <>
      <LibraryViewContext.Provider value={{ books, setBooks }}>
        <Library></Library>
      </LibraryViewContext.Provider>
    </>
  );
};

export default LibraryView;
