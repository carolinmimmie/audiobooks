import { fabClasses } from "@mui/material";
import {
  addDoc,
  collection,
  deleteDoc,
  doc,
  getDocs,
  query,
  updateDoc,
  where,
} from "firebase/firestore";
import { db } from "../../../firestore-config";
import { ILibraryBook } from "../../interfaces";

//Skapar en referense till min db = databas som heter allBooks
const booksCollectionRef = collection(db, "books");

export const getBooksStatusFalse = async () => {
  const q = query(booksCollectionRef, where("liked", "==", false));
  // query = en förfrågan mot bak-end som en filtrering vart den ska kolla where är villkoret med två villkor
  //hämta data utifrån en villkor
  //getDocs() en metod för hämta all data
  const data = await getDocs(q);
  return (
    //Gör om datat så det blir läsbart
    data.docs.map((doc) => ({
      ...(doc.data() as ILibraryBook),
      //Det behövs typas då det är typescript
      //id kommer inte med då det inte är en del av obejktet i fältet i firebase
      id: doc.id,
    }))
  );
};
export const updateBook = async (x: ILibraryBook) => {
  await updateDoc(doc(booksCollectionRef, x.id), {
    liked: !x.liked,
  });
};
