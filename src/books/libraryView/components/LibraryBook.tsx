import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { IBook, ILibraryBook } from "../../interfaces";
interface IProps {
  book: ILibraryBook;
  makeLike(book: ILibraryBook): void;
}
function LibraryBook({ book, makeLike }: IProps) {
  return (
    <>
      <Card sx={{ minWidth: 180, minHeight: 230 }} className="li-book">
        <CardContent className="library-book">
          <Typography sx={{ fontSize: 15 }}>{book.title}</Typography>
        </CardContent>
        <CardActions className="actions" style={{ padding: 2 }}>
          <CardContent className="library-author">
            <Typography sx={{ fontSize: 15 }}>{book.author}</Typography>
          </CardContent>
          <div className="liked-icon">
            <Button
              style={{ color: book.liked ? "red" : "grey" }}
              onClick={() => makeLike(book)}
              size="small"
            >
              <FavoriteIcon />
            </Button>
          </div>
        </CardActions>
      </Card>
    </>
  );
}

export default LibraryBook;
