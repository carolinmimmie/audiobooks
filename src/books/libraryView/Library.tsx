import React, { useContext } from "react";
import { ILibraryBook } from "../interfaces";
import { updateBook } from "../mainView/ManViewApi";
import LibraryBook from "./components/LibraryBook";
import { LibraryViewContext } from "./components/LibraryView";
import { getBooksStatusFalse } from "./components/LibraryViewApi";

function Library() {
  const { books, setBooks } = useContext(LibraryViewContext);
  
  const makeLike = async (book: ILibraryBook) => {
    await updateBook(book);
    const newBooks = await getBooksStatusFalse();
    setBooks(newBooks);
  };


  const book = books.map((x) => (
    <LibraryBook book={x} makeLike={makeLike}></LibraryBook>
  ));

  return (
    <div className="bg">
      <section className="border">
        <div className="title">
          <h1>Hitta nått att läsa:</h1>
        </div>
        <div className="center">
          <div className="lib-books-container">{book}</div>
        </div>
      </section>
    </div>
  );
}

export default Library;
