import React, { useState } from "react";
import Dialog from "@mui/material/Dialog";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { IAudio, IBook } from "../interfaces";

interface IProps {
  book: IBook;
  handleClickOpen: () => void;
  handleClose: () => void;
  open: boolean;
}

const AudioPlayerDialog = ({ book, handleClose, open }: IProps) => {
  return (
    <div className="dialog">
      <Dialog open={open}>
        <div className="audio-player">
          <div className="close-button">
            <Button></Button>
          </div>
          <Typography variant="h5" onClick={handleClose}>
            {book.title}
          </Typography>
          <Typography variant="subtitle1">{book.author}</Typography>
          <hr></hr>
          <audio
            src="https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba.mp3"
            controls={true}
          />
        </div>
      </Dialog>
    </div>
  );
};

export default AudioPlayerDialog;
