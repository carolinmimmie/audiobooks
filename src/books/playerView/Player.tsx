import { getDoc } from "@firebase/firestore";
import { Button, Dialog, Typography } from "@mui/material";
import { collection, doc } from "firebase/firestore";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { db } from "../../firestore-config";
import { IAudio } from "../interfaces";

const Player = () => {
  const [isPlaying, setIsPlaying] = useState(false);
  const [book, setBook] = useState<IAudio>({
    id: "",
    title: "",
    author: "",
    audioUrl: "",
  });
  const booksCollectionRef = collection(db, "books");

  let { id } = useParams();

  //definer upp en fetch som hämtar id:et
  const getBook = async () => {
    const data = await getDoc(doc(booksCollectionRef, id));
    setBook({ id: data.id, ...data.data() } as IAudio);
  };
  useEffect(() => {
    getBook();
  }, []);
  return (
    <div className="player">
      <div className="audio-player">
        {/* <Dialog open={open}>
            <div className="audio-player"> */}
        <div className="close-button">
          <Button></Button>
        </div>
        {/* <Typography variant="h5" onClick={handleClose}></Typography> */}
        <Typography variant="h5">{book.title}</Typography>
        <Typography variant="subtitle1">{book.author}</Typography>
        <hr></hr>
        <audio
        onPlay = {() => setIsPlaying(true)}
        onPause = {() => setIsPlaying(false)}
        src={book.audioUrl} controls={true} />
      </div>
    </div>
  );
};

export default Player;
