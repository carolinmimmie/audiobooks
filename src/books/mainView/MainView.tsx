import Main from "./Main";
import { Button } from "@mui/material";
import { formLabelClasses } from "@mui/material";

import {
  addDoc,
  collection,
  deleteDoc,
  doc,
  getDocs,
  query,
  updateDoc,
  where,
} from "firebase/firestore";
import React, { createContext, Dispatch, useEffect, useState } from "react";
import { db } from "../../firestore-config";
import { IBook } from "../interfaces";
import Book from "./components/Book";
import { getBooksStatusTrue } from "./ManViewApi";
//Interface för mitt context
interface IMainViewContext {
  books: IBook[];
  setBooks: Dispatch<React.SetStateAction<IBook[]>>;
  title: string;
  setTitle: React.Dispatch<React.SetStateAction<string>>;
  author: string;
  setAuthor: React.Dispatch<React.SetStateAction<string>>;
}

//Skapa context
export const MainViewContext = createContext<IMainViewContext>({
  books: [],
  setBooks: () => {},
  title: "",
  setTitle: () => {},
  author: "",
  setAuthor: () => {},
});
const MainView = () => {
  const [books, setBooks] = useState<IBook[]>([]);
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");

  const fetchBooks = async () => {
    //KALLA på api getBooksStatusTrue
    setBooks(await getBooksStatusTrue());
  };
  //Min useeffect på parent nivå.
  useEffect(() => {
    fetchBooks();
    //and an array of dependencies that make the effect fire.
  }, []);

  // skapar en komponent vars avsikt enbart är att visa upp parents och wrappa den i ett context
  return (
    <>
      <MainViewContext.Provider
        value={{ books, setBooks, title, setTitle, author, setAuthor }}
      >
        <Main></Main>
      </MainViewContext.Provider>
    </>
  );
};

export default MainView;
