import {
  addDoc,
  collection,
  deleteDoc,
  doc,
  getDocs,
  query,
  updateDoc,
  where,
} from "firebase/firestore";
import { useContext } from "react";

//FIL FÖR ALLA API TILL DATABAS

import { db } from "../../firestore-config";
import { IBook } from "../interfaces";
import { MainViewContext } from "./MainView";
//Skapar en referense till min db = databas som heter allBooks
const booksCollectionRef = collection(db, "books");

export const getBooksStatusTrue = async () => {
  const q = query(booksCollectionRef, where("liked", "==", true));
  // query = en förfrågan mot bak-end som en filtrering vart den ska kolla where är villkoret med två villkor
  //hämta data utifrån en villkor
  //getDocs() en metod för hämta all data
  const data = await getDocs(q);
  return (
    //Gör om datat så det blir läsbart
    data.docs.map((doc) => ({
      ...(doc.data() as IBook),
      //Det behövs typas då det är typescript
      //id kommer inte med då det inte är en del av obejktet i fältet i firebase
      id: doc.id,
    }))
  );
};

export const updateBook = async (book: IBook) => {
  await updateDoc(doc(booksCollectionRef, book.id), {
    liked: !book.liked,
  });

  getBooksStatusTrue();
};

export const aBook = async (book: IBook) => {
  // Stoppa in våran refernce och det nya objekt vi vill skapa
  await addDoc(booksCollectionRef, {
    title: book.title,
    author: book.author,
    liked: book.liked,
  });

  //tillåter mig att trigga en uppdatering av ett state
};

export const dBook = async (id: string) => {
  await deleteDoc(doc(booksCollectionRef, id));
};
