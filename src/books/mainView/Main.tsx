import { Button } from "@mui/material";
import React, { useContext, useState } from "react";
import { IBook } from "../interfaces";
import Book from "./components/Book";
import { MainViewContext } from "./MainView";
import { aBook, dBook, getBooksStatusTrue, updateBook } from "./ManViewApi";

function Main() {
  const { books, setBooks, title, setTitle, author, setAuthor } =
    useContext(MainViewContext);

  const makeLike = async (book: IBook) => {
    // const fetchBooks = async () => {
    await updateBook(book);
    // };
    // fetchBooks();

    setBooks(await getBooksStatusTrue());
  };
  const deleteBook = async (id: string) => {
    // const fetchBooks = async () => {
    await dBook(id);
    // };
    // fetchBooks();
    setBooks(await getBooksStatusTrue());
  };
  const addBook = async (book: IBook) => {
    // const fetchBooks = async () => {
    await aBook(book);
    // };
    // fetchBooks();
    setBooks(await getBooksStatusTrue());
  };
  const [formData, setFormData] = useState<IBook>({
    //Satt mina use-state variablar till tomma som original
    id: "",
    title: "",
    author: "",
    liked: true,
  });

  //Känner av att något händer i input-fältet
  const handleChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name } = event.target;
    setFormData({ ...formData, [name]: event.target.value });
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event?.preventDefault();
    addBook(formData);
  };
  const book = books.map((x) => (
    <Book book={x} makeLike={makeLike} deleteBook={deleteBook}></Book>
  ));
  return (
    <div className="bg">
      <section className="border">
        <form className="form" onSubmit={handleSubmit}>
          <div className="form-box">
            <label htmlFor="title">Titel:</label>
          </div>
          <div className="form-box">
            <input
              type="text"
              id="title"
              name="title"
              value={formData.title}
              onChange={handleChange}
            />
          </div>
          <div className="form-box">
            <label htmlFor="author">Författare:</label>
          </div>
          <div className="form-box">
            <input
              type="text"
              id="author"
              name="author"
              value={formData.author}
              onChange={handleChange}
            />
          </div>

          <div className="form-box">
            <button type="submit" className="submit-button">
              Lägg till bok{" "}
            </button>
          </div>
        </form>

        <div className="books-container">{book}</div>
        <div className="box">
          <p>
            <span className="left">"</span> du är aldrig ensam
          </p>
          <p>
            när du läser <span className="right"> " </span>
          </p>
        </div>
      </section>
    </div>
  );
}

export default Main;
