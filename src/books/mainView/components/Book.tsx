import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import PlayCircleIcon from "@mui/icons-material/PlayCircle";
import FavoriteIcon from "@mui/icons-material/Favorite";
import AudioPlayerDialog from "../../playerView/AudioPlayerDialog";
import { IBook } from "../../interfaces";
import { useNavigate } from "react-router-dom";

interface IProps {
  book: IBook;
  makeLike(book: IBook): void;
  deleteBook(id: string): void;
}
function Book({ book, makeLike, deleteBook }: IProps) {
  const [open, setOpen] = React.useState(false);
  let navigate = useNavigate();


  //Skicka med id-strängen  med params till component player
  const handleClickOpen = () => {
    // setOpen(true);
    navigate(`/player/${book.id}`);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Card className="book">
        <CardContent className="book-card-img">
          <Typography sx={{ fontSize: 20 }}>{book.title}</Typography>
          <Typography sx={{ fontSize: 15 }}>{book.author}</Typography>
        </CardContent>
        <CardActions className="actions">
          <Button onClick={handleClickOpen}>
            <PlayCircleIcon className="color" />
          </Button>
          <Button
            size="small"
            style={{ color: book.liked ? "red" : "grey" }}
            onClick={() => makeLike(book)}
          >
            <FavoriteIcon />
          </Button>
          <Button onClick={() => deleteBook(book.id)}>Ta bort</Button>
        </CardActions>
      </Card>
      <AudioPlayerDialog
        handleClose={handleClose}
        book={book}
        handleClickOpen={handleClickOpen}
        open={open}
      />
    </>
  );
}

export default Book;
