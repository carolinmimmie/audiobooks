import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Header from "./books/Header";
import Main from "./books/mainView/Main";
import Library from "./books/libraryView/Library";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Player from "./books/playerView/Player";
import MainView from "./books/mainView/MainView";
import LibraryView from "./books/libraryView/components/LibraryView";
function App() {
  return (
    <BrowserRouter>
      <Header></Header>
      <Routes>
        <Route path="/" element={<MainView />}></Route>
        <Route path="/mainview" element={<MainView />}></Route>
        <Route path="/library" element={<LibraryView />}></Route>
        <Route path="/player/:id" element={<Player />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
